#pragma once
#include "player.h"


Snake::Snake()
{
}
Snake::~Snake()
{

}

void Snake::liveBody()
{
	sizeSquare = 20;
	sf::RectangleShape squareSnake;
	squareSnake.setSize(sf::Vector2f(sizeSquare, sizeSquare));

	if (squareBody.empty())
	{
		squareSnake.setPosition(posX, posY);
		squareSnake.setFillColor(sf::Color::Cyan);
		squareSnake.setOutlineColor(sf::Color::Red);
		//squareSnake.setOutlineThickness(1);
		std::cout << "player pos" << posX << posY << std::endl;
	}
	else
	{
		//Initialize next square snake position from the precedent square snake position and size (x pos - size, y position)
		squareSnake.setPosition((squareBody[squareBody.size() - 1].getPosition().x - squareBody[squareBody.size() - 1].getSize().x), squareBody[squareBody.size() - 1].getPosition().y);
	}
	squareBody.push_back(squareSnake);
}

sf::RectangleShape Snake::spawnFood()
{
	int  foodSize = 20;
	sizeSquare = 20;

	food.setSize(sf::Vector2f(foodSize, foodSize));
	food.setFillColor(sf::Color::Magenta);

	//circleFood.setOutlineThickness(1);
	//food.setOrigin(food.getGlobalBounds().width / 2, food.getGlobalBounds().height / 2);
	food.setPosition(foodRandxPos, foodRandyPos);

	collisionFood = food.getGlobalBounds();

	return food;
}

void Snake::setupSeed(int screenW, int screenH, int levelW, int levelH)
{
	int  foodSize = 20.f;

	
	foodRandxPos = ((levelW + foodSize*2)/20 + rand() % ((screenW - (2 * levelW) - foodSize)/20)) * 20 -20;
	foodRandyPos = ((levelH + foodSize*2) / 20 + rand() % ((screenH - (2 * levelH) - foodSize) / 20)) * 20 - 20;

	std::cout << "DEBUG: food random position (" << foodRandxPos << "," << foodRandyPos << ")" << std::endl;
}


// std::cout << << std::endl;