#pragma once
#include "game.h"


class Snake
{
public:
	Snake();
	virtual ~Snake();

	//Function
	//sf::CircleShape setupSnake();
	void liveBody();
	void setupSeed(int screenW, int screenH, int levelW, int levelH);



	sf::RectangleShape spawnFood();

	std::vector<sf::RectangleShape> squareBody;
	sf::RectangleShape food;

	float sizeSquare;

	sf::FloatRect collisionFood;

	//Variable
	float levelOneWidth;
	float levelOneHeight;

	int rotator;

	float posX;
	float posY;
	
	float foodRandxPos;
	float foodRandyPos;
	

private:
};