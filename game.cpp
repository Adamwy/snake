#pragma once
#include "game.h"

Snake snake;

game::game()
{
	video();
	firstLevel();
	initialize();
	menu();
	updateEvent();
	Collision();
}

game::~game()
{
}



void game::update()
{
	window->clear();
	updateEvent();
	setupScore();
	window->draw(welcomeText);
	window->draw(levelOne);
	window->draw(snake.spawnFood());
	window->draw(actualScore);
	window->draw(highscore);


	if (startGame && !gameOver)
	{
		setupMovement();
	}
	else if (gameOver)
	{
		restartGame();
	}

	snake.rotator = ++snake.rotator;

	for (int index = 0; index < snake.squareBody.size(); index++)
	{
		window->draw(snake.squareBody[index]);
	}
}

void game::render()
{
	window->display();
}



void game::video()
{
	// Setting up windows
	screen.width = (800);
	screen.height = (600);

	window = new sf::RenderWindow(screen, "Snake C++/SFML", sf::Style::Default);
	window->setFramerateLimit(165);
	window->clear(sf::Color::Black);

	std::cout << "DEBUG: video setting initialized !" << std::endl;
}
void game::firstLevel()
{
	// Setting up level one
	levelOne = sf::VertexArray(sf::LineStrip, 5);

	// Level one size
	levelOneWidth = (screen.width / 4) - 4;
	levelOneHeight = (screen.height / 5) - 4;

	snake.levelOneWidth = levelOneWidth;
	snake.levelOneHeight = levelOneHeight;

	// Creation of the rectangle
	levelOne[0].position = sf::Vector2f(levelOneWidth, levelOneHeight);
	levelOne[1].position = sf::Vector2f(screen.width - levelOneWidth, levelOneHeight);
	levelOne[2].position = sf::Vector2f(screen.width - levelOneWidth, screen.height - levelOneHeight);
	levelOne[3].position = sf::Vector2f(levelOneWidth, screen.height - levelOneHeight);
	levelOne[4].position = sf::Vector2f(levelOneWidth, levelOneHeight);


	std::cout << "DEBUG: level one created !" << std::endl;
}
void game::initialize()
{
	// Setting up game mode
	gameOver = false;
	startGame = false;
	moveRight = true;
	moveLeft = false;
	moveUp = false;
	moveDown = false;

	score = 0;

	saveFile = "save.txt";

	save.open(saveFile);
	save >> maxScore;
	save.close();

	snake.posX = screen.width / 2;
	snake.posY = screen.height / 2;

	// Initialize time

	horloge.restart();

	// Default Snake lives
	snake.liveBody();
	snake.liveBody();
	snake.liveBody();


	// Setup first random food seed
	snake.setupSeed(screen.width, screen.height, levelOneWidth, levelOneHeight);

	std::cout << "DEBUG: root configuration loaded !" << std::endl;
}
void game::menu()
{
	//Getting font from file
	welcomeFont.loadFromFile("arial.ttf");
	//Set up text font
	welcomeText.setFont(welcomeFont);
	//Welcoming string
	welcomeText.setString("Snake");
	//Char size
	welcomeText.setCharacterSize(24);
	//Text colour
	welcomeText.setFillColor(sf::Color::White);
	//Text origin to center of the text
	welcomeText.setOrigin((welcomeText.getLocalBounds().width / 2.0f), (welcomeText.getLocalBounds().height / 2.0f));
	//Text position
	welcomeText.setPosition(screen.width / 2.0f, 20);



	std::cout << "DEBUG: menu initialized!" << std::endl;
}
void game::updateEvent()
{
	while (this->window->pollEvent(input))
	{
		switch (input.type)
		{
		case sf::Event::Closed:
			window->close();
			break;

		case sf::Event::KeyPressed:
			direction(input.key.code);
			if (input.key.code == sf::Keyboard::Escape)
			{
				window->close();
				break;
			}
			if (input.key.code == sf::Keyboard::Enter)
			{
				startGame = true;
				break;
			}
			if (input.key.code == sf::Keyboard::E)
			{
				snake.liveBody();
				break;
			}
			if (input.key.code == sf::Keyboard::F)
			{
				snake.setupSeed(screen.width, screen.height, levelOneWidth, levelOneHeight);
				break;
			}
		}
	}
}


void game::direction(float playerInput)
{
	if (playerInput == sf::Keyboard::Up && !moveDown)
	{	
		moveUp = true;
		moveDown = false;
		moveLeft = false;
		moveRight = false;
	}
	if (playerInput == sf::Keyboard::Down && !moveUp)
	{
		moveUp = false;
		moveDown = true;
		moveLeft = false;
		moveRight = false;
	}
	if (playerInput == sf::Keyboard::Left && !moveRight)
	{
		moveUp = false;
		moveDown = false;
		moveLeft = true;
		moveRight = false;
	}
	if (playerInput == sf::Keyboard::Right && !moveLeft)
	{
		moveUp = false;
		moveDown = false;
		moveLeft = false;
		moveRight = true;
	}
}

void game::setupScore()
{
	// Store max score in save.txt
	if (score > maxScore)
	{
		save.open(saveFile);
		maxScore = score;
		save << maxScore;
		save.close();
		std::cout << "DEBUG: maxscore: " << maxScore << std::endl;
	}
	yourScore = "Score : " + std::to_string(score);

	//Getting font from file
	welcomeFont.loadFromFile("arial.ttf");
	//Set up text font
	actualScore.setFont(welcomeFont);
	//Welcoming string
	actualScore.setString(yourScore);
	//Char size
	actualScore.setCharacterSize(20);
	//Text colour
	actualScore.setFillColor(sf::Color::White);
	//Text origin to center of the text
	actualScore.setOrigin((actualScore.getLocalBounds().width / 2.0f), (actualScore.getLocalBounds().height / 2.0f));
	//Text position
	actualScore.setPosition(levelOneWidth + 40, levelOneHeight - 20);

	//Get highscore
	save.open(saveFile);
	save >> yourHighscore;
	save.close();

	textHighscore = "Highscore : " + yourHighscore;

	//Set up text font
	highscore.setFont(welcomeFont);
	//Welcoming string
	highscore.setString(textHighscore);
	//Char size
	highscore.setCharacterSize(20);
	//Text colour
	highscore.setFillColor(sf::Color::White);
	//Text origin to center of the text
	highscore.setOrigin((actualScore.getLocalBounds().width / 2.0f), (actualScore.getLocalBounds().height / 2.0f));
	//Text position
	highscore.setPosition(screen.width - levelOneWidth - 90 , levelOneHeight - 20);
}

void game::Collision()
{
	collisionHead = snake.squareBody[0].getGlobalBounds();
	collisionNeck = snake.squareBody[1].getGlobalBounds();

	//Detect collision between head and body
	for (int index = 1; index < snake.squareBody.size() - 1; index++)
	{
		collisionBody = snake.squareBody[index].getGlobalBounds();

		if (collisionHead.intersects(collisionBody))
		{
			gameOver = true;
			std::cout << "DEBUG: body loose!" << std::endl;
		}

	}
	//Verify collision with food
	if (snake.squareBody[0].getPosition().x == snake.food.getPosition().x && snake.squareBody[0].getPosition().y == snake.food.getPosition().y)
	{
		snake.liveBody();
		snake.setupSeed(screen.width, screen.height, levelOneWidth, levelOneHeight);
		snake.spawnFood();
		++score;
		std::cout << "WIN" << std::endl;
	}

	//Collision with level on x axis
	if (snake.squareBody[0].getPosition().x + snake.sizeSquare > screen.width - levelOneWidth || snake.squareBody[0].getPosition().x < levelOneWidth)
	{
		gameOver = true;
		std::cout << "LOOSE" << std::endl;
	}

	// collision with level on y axis
	if (snake.squareBody[0].getPosition().y + snake.sizeSquare > screen.height - levelOneHeight || snake.squareBody[0].getPosition().y < levelOneHeight)
	{
		gameOver = true;
		std::cout << "LOOSE" << std::endl;
	}
}

void game::restartGame()
{
	window->clear();
	snake.squareBody.clear();
	initialize();
}


void game::setupMovement()
{
	// initiate tima
	temps = horloge.getElapsedTime();

	timeTick = 100.0f;

	if (temps.asMilliseconds() < timeTick)
	{
		return;
	}
	else
	{
		// Moove snake[index+1] to snake[index] position, then moove snake[0]
		for (int index = snake.squareBody.size() - 1; index > 0; index--)
		{
			// Creation of space between snake square for collision utility

			snake.squareBody[index].setPosition(snake.squareBody[index - 1].getPosition().x, snake.squareBody[index - 1].getPosition().y);
		}

		if (moveUp)

		{
			snake.squareBody[0].move(0, -snake.sizeSquare);
		}
		if (moveDown)

		{
			snake.squareBody[0].move(0, snake.sizeSquare);
		}
		if (moveLeft)

		{
			snake.squareBody[0].move(-snake.sizeSquare, 0);
		}
		if (moveRight)

		{
			snake.squareBody[0].move(snake.sizeSquare, 0);
		}
	}
	temps = horloge.restart();
	Collision();

}

const bool game::WindowOpen() const
{
	return window->isOpen();
}

// std::cout << << std::endl;