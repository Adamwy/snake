#pragma once
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Network.hpp>

#include <iostream>
#include <ctime>
#include <string>
#include <fstream>


#include "player.h"



class game
{
public:
	game();
	virtual ~game();

	//Main function
	void update();
	void render();

	//Main lopp
	const bool WindowOpen() const;

	
private:

	//Private variable

	//Window & event
	sf::RenderWindow* window;
	sf::VideoMode screen;
	sf::Event input;

	//Collision variable
	sf::FloatRect collisionHead;
	sf::FloatRect collisionNeck;
	sf::FloatRect collisionBody;
	sf::FloatRect collisionLevel;
	sf::FloatRect collisionFood;

	//Moove variable
	bool moveUp;
	bool moveDown;
	bool moveLeft;
	bool moveRight;

	//Drawed welcome text
	sf::Text welcomeText;
	sf::Font welcomeFont;

	//Score text
	sf::Text actualScore;
	sf::Text highscore;
	std::string yourScore;
	std::string yourHighscore;
	std::string textHighscore;

	//Level one
	float levelOneWidth;
	float levelOneHeight;
	sf::VertexArray levelOne;

	//Time variable
	sf::Time temps;
	sf::Clock horloge;
	float timeTick;

	//Is the game started/over?
	bool startGame;
	bool gameOver;

	// Save score
	int score;
	int maxScore;	
	std::fstream save;
	std::string saveFile;


	//Private Function

	void video();
	void initialize();
	void firstLevel();
	void menu();
	void updateEvent();

	void setupMovement();
	void direction(float playerInput);
	void Collision();
	void restartGame();
	void setupScore();

};

