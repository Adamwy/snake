#pragma once
#include "game.h"
#include "player.h"



int main()
{
	srand(time(NULL));
	game snakeGame;

	while (snakeGame.WindowOpen())
	{

		// update the game
		snakeGame.update();

		// render the game
		snakeGame.render();
	}

	return 0;
}